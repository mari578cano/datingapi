﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DatingApp.API.IServices { 
    public interface ITokenService
    {
        string CreateToken(User user);
    }
}
